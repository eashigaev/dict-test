<?php

use yii\db\Migration;

/**
 * Handles the creation of table `words`.
 */
class m161023_150207_create_word_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('word', [
            'id' => $this->primaryKey(),
            'eng' => $this->string(),
            'rus' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('word');
    }
}
