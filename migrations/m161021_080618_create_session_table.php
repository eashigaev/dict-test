<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m161021_080618_create_session_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('session', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'token' => $this->string()->notNull()->unique(),
            'test' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex
        (
            'idx-session-token',
            'session',
            'token'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('session');
    }
}
