<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mistakes`.
 */
class m161030_132956_create_mistake_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mistake', [
            'id' => $this->primaryKey(),
            'question' => $this->string(),
            'answers' => $this->text(),
            'number' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mistake');
    }
}
