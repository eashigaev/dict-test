<?php

namespace app\controllers;

use app\models\Word;
use Layers\Application\Commands\AnswerCommand;
use Layers\Application\Commands\QuestionCommand;
use Layers\Application\Commands\StartCommand;
use Layers\Infrastructure\Application\Commands\Contracts\CommandBusInterface;
use Yii;
use yii\base\Exception;
use yii\web\ForbiddenHttpException;

class ApiController extends \yii\web\Controller
{

    private
        $commandBus;

    public function __construct($id, $module, CommandBusInterface $commandBus, $config = [])
    {
        $this->commandBus = $commandBus;

        Yii::$app->response->format = 'json';

        $this->enableCsrfValidation = false;

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $wordsCount = Word::find()->count();

        if ($wordsCount) return 'Data... OK';

        $data =
            [
                "apple" => "яблоко",
                "pear" => "персик",
                "orange" => "апельсин",
                "grape" => "виноград",
                "lemon" => "лимон",
                "pineapple" => "ананас",
                "watermelon" => "арбуз",
                "coconut" => "кокос",
                "banana" => "банан",
                "pomelo" => "помело",
                "strawberry" => "клубника",
                "raspberry" => "малина",
                "melon" => "дыня",
                "apricot" => "абрикос",
                "mango" => "манго",
                "pear" => "слива",
                "pomegranate" => "гранат",
                "cherry" => "вишня"
            ];

        foreach ($data as $rus => $eng)
        {
            $item = new Word();

            $item->rus = $rus;

            $item->eng = $eng;

            $item->save();
        }

        return 'Seed... OK';
    }

    public function actionStart($name)
    {
        return $this->sandbox
        (
            function() use ($name)
            {
                $command = new StartCommand($name);

                return $this->commandBus->execute($command);
            }
        );
    }

    public function actionQuestion($token)
    {
        return $this->sandbox
        (
            function() use ($token)
            {
                $command = new QuestionCommand($token);

                return $this->commandBus->execute($command);
            }
        );
    }

    public function actionAnswer($number, $token)
    {
        return $this->sandbox
        (
            function() use ($number, $token)
            {
                $command = new AnswerCommand($number, $token);

                return $this->commandBus->execute($command);
            }
        );
    }

    protected function sandbox($callback)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try
        {
            $result = $callback();

            $transaction->commit();

            return $result;
        }
        catch (Exception $e)
        {
            $transaction->rollback();

            throw new ForbiddenHttpException();
        }
    }

}
