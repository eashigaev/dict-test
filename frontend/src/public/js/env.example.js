(function (window) {
    window.__env = window.__env || {};
    window.__env.apiUrl = 'http://localhost:8081/api';
    window.__env.enableDebug = false;
}(this));