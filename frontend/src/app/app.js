import '../style/app.scss';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './app.config';
import bootstrap from './app.bootstrap';

import dict from './components/dict';
import form from './components/form';

import utils from './shared/utils';

angular.module('app', [
    uirouter,
    utils,
    dict,
    form
])
    .config(routing)
    .run(bootstrap);

