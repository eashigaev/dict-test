import angular from "angular";
import RegistryService from "./registry.service";
import ApiService from "./api.service";

export default angular.module('app.shared.utils', [])
    .service('registryService', RegistryService)
    .service('apiService', ApiService)
    .name;