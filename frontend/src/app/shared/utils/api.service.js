export default class {

    constructor($http, registryService) {
        'ngInject';

        this.apiUrl = registryService.get('apiUrl');
        this.$http = $http;
    }

    get(url, params) {
        return this.$http
            .get(this.apiUrl + url, {params})
            .then(::this.parseResponse);
    }

    post(url, params) {
        return this.$http
            .post(this.apiUrl + url, params)
            .then(::this.parseResponse);
    }

    parseResponse(res) {
        return res.data;
    }

};
