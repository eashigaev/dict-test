export default class RegistryService {

    constructor() {
        'ngInject';

        this.data = {};
    }

    get(key, defaultValue) {
        return this.data[key] || defaultValue;
    }

    set(key, value) {
        return this.data[key] = value;
    }

    getAll() {
        return {...this.data};
    }

    setAll(data) {
        this.data = {...this.data, ...data}
    }

}
