import templateUrl from 'form/main.html';

export default function routes($stateProvider) {
    'ngInject';
    $stateProvider
        .state('form', {
            url: '/form',
            views: {
                'body@': {
                    templateUrl
                }
            }
        });
};
