import templateUrl from "form/form.html";

class controller {

    constructor()
    {
        'ngInject';

        this.seed();
    }

    seed()
    {

        this.specs = [
            'Программист',
            'Парикмахер',
            'Адвокат',
            'Каменщик',
            'Парикмахер-визажист'
        ];

        this.phones = [
            {
                name: 'Alabama',
                flag: '5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png',
                code: '+1'
            },
            {
                name: 'Alaska',
                flag: 'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png',
                code: '+2'
            },
            {
                name: 'Arizona',
                flag: '9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png',
                code: '+3'
            }
        ];

    }

}

export default {
    templateUrl,
    controller
};