import angular from "angular";
import uirouter from "angular-ui-router";
import uibootstrap from "angular-ui-bootstrap";

import routing from "./routes";

import formComponent from "./form.component";

export default angular.module('app.form', [
    uirouter, uibootstrap
])
    .config(routing)
    .component('formTest', formComponent)
    .name;
