import apiService from '../../shared/utils/api.service'

export default class extends apiService {

    start(name) {
        return this.get('/start/', { name });
    }

    question(token) {
        return this.get('/question/', { token });
    }

    answer(number, token) {
        return this.get('/answer/', { number, token });
    }

};
