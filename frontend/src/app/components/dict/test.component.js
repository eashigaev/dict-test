import templateUrl from 'dict/test.html';

class controller {

    constructor(dictApiService) {
        'ngInject';

        this.apiService = dictApiService;
        this.name = 'Tester';
        this.unlock();
    }

    callStart() {
        if (!this.name || this.locked()) return false;

        this.lock();
        return this.apiService.start(this.name).then((res) => {
            Object.assign(this, res);
            return this.callQuestion();
        });
    }

    callQuestion() {
        return this.apiService.question(this.token).then((res) => {
            Object.assign(this, res);
            this.unlock();
        });
    }

    callAnswer(number) {
        if (this.locked()) return false;

        this.lock();
        return this.apiService.answer(number, this.token).then((res) => {
            Object.assign(this, res);
            return this.callQuestion();
        });
    }

    locked() {
        return this._lock === true;
    }

    lock() {
        this._lock = true;
    }

    unlock() {
        this._lock = false;
    }

}

export default {
    templateUrl,
    controller,
    bindings: {

    }
};