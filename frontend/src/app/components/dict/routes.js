import templateUrl from 'dict/dict.html';

export default function routes($stateProvider) {
    'ngInject';
    $stateProvider
        .state('base', {
            url: '/',
            views: {
                'body@': {
                    templateUrl,
                    controller: 'DictPageController',
                    controllerAs: '$ctrl',
                    bindToController: true
                }
            }
        });
};
