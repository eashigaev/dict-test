import angular from "angular";
import uirouter from "angular-ui-router";

import routing from "./routes";

import dictApiService from "./api.service";

import PageController from "./page.controller";
import dictTestComponent from "./test.component";

export default angular.module('app.dict', [
    uirouter
])
    .config(routing)
    .service('dictApiService', dictApiService)
    .controller('DictPageController', PageController)
    .component('dictTest', dictTestComponent)
    .name;
