<?php


namespace Layers\Application\Commands;

class AnswerCommand extends AuthCommand
{

    public
        $number;

    public function __construct($number, $token)
    {
        $this->number = $number;

        parent::__construct($token);
    }

}