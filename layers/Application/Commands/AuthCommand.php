<?php


namespace Layers\Application\Commands;

use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

abstract class AuthCommand implements CommandInterface
{

    public
        $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

}