<?php


namespace Layers\Application\Commands;

use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

class StartCommand implements CommandInterface
{

    public
        $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

}