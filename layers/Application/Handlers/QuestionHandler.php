<?php


namespace Layers\Application\Handlers;

use app\models\Session;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

class QuestionHandler extends AbstractHandler
{

    public function handle(CommandInterface $command)
    {
        $token = $command->token;

        $session = Session::findByTokenOrFail($token);

        $test = $session->getTest();

        return
        [
            'question' => $test['question'],

            'answers' => $test['answers'],

            'status' => $test['status'],

            'points' => $test['points']
        ];
    }
}