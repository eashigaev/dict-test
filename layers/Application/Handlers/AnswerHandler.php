<?php


namespace Layers\Application\Handlers;

use Yii;
use app\models\Session;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

class AnswerHandler extends AbstractHandler
{

    public function handle(CommandInterface $command)
    {
        $token = $command->token;

        $session = Session::findByTokenOrFail($token);


        $number = $command->number;

        $test = $session->getTest();

        $test->takeAnswer($number);

        $session->saveTest($test);


        $session->save();

        return
        [
            'result' => $test['lastAnswered'],

            'status' => $test['status'],

            'points' => $test['points']
        ];
    }
}