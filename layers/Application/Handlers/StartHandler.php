<?php


namespace Layers\Application\Handlers;

use app\models\Session;
use app\models\Word;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

class StartHandler extends AbstractHandler
{

    public function handle(CommandInterface $command)
    {
        $name = $command->name;


        $session = new Session();


        $session->name = $name;

        $session->token = Session::generateToken();


        $test = $session->getTest();

        $test->nextQuestion();

        $session->saveTest($test);


        $session->save();


        return [
            'token' => $session->token,

            'status' => $test['status'],

            'points' => $test['points']
        ];
    }
}