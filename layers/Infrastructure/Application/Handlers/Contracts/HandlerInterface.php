<?php

namespace Layers\Infrastructure\Application\Handlers\Contracts;

use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

interface HandlerInterface
{

    public function handle(CommandInterface $command);

}