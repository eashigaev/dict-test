<?php

namespace Layers\Infrastructure\Application\Commands;

use Layers\Infrastructure\Application\Commands\Contracts\CommandInflectorInterface;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;

class NameCommandInflector implements CommandInflectorInterface
{

    public function getHandlerClass(CommandInterface $command)
    {
        return str_replace('Command', 'Handler', get_class($command));
    }

}