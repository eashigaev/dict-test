<?php

namespace Layers\Infrastructure\Application\Commands;

use Layers\Infrastructure\Application\Commands\Contracts\CommandBusInterface;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInflectorInterface;
use Layers\Infrastructure\Application\Commands\Contracts\CommandInterface;
use Layers\Infrastructure\Application\Containers\Contracts\ContainerInterface;

class CommandBus implements CommandBusInterface
{

    protected
        $container,
        $inflector;

    public function __construct(ContainerInterface $container, CommandInflectorInterface $inflector)
    {
        $this->container = $container;
        $this->inflector = $inflector;
    }

    public function execute(CommandInterface $command)
    {
        return $this->resolveHandler($command)->handle($command);
    }

    public function resolveHandler(CommandInterface $command)
    {
        $handler = $this->inflector->getHandlerClass($command);
        return $this->container->make($handler);
    }
}