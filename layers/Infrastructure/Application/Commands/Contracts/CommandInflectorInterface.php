<?php

namespace Layers\Infrastructure\Application\Commands\Contracts;

interface CommandInflectorInterface
{

    public function getHandlerClass(CommandInterface $command);

}