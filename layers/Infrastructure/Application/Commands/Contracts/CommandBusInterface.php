<?php

namespace Layers\Infrastructure\Application\Commands\Contracts;

interface CommandBusInterface
{

    public function execute(CommandInterface $command);

    public function resolveHandler(CommandInterface $command);

}