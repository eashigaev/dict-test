<?php

namespace Layers\Infrastructure\Application\Containers;

use Layers\Infrastructure\Application\Containers\Contracts\ContainerInterface;

class Yii2Container implements ContainerInterface
{

    public function make($class)
    {
        return \Yii::$container->get($class);
    }

}