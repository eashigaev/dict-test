<?php

namespace Layers\Infrastructure\Application\Containers\Contracts;

interface ContainerInterface
{

    public function make($class);

}