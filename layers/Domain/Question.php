<?php


namespace Layers\Domain;

use app\models\Word;

class Question
{

    private
        $from,
        $to,

        $id,
        $text,
        $answers,
        $answerNumber;

    public function getId()
    {
        return $this->id;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function getAnswerNumber()
    {
        return $this->answerNumber;
    }

    public function __construct($from, $to)
    {
        $this->from = $from;

        $this->to = $to;
    }

    public function setText(Word $word)
    {
        $this->id = $word->id;

        $this->text = $word->{$this->from};
    }

    public function setAnswers(array $words)
    {
        $answers = [];

        foreach ($words as $word)
        {
            $answers[] = $word->{$this->to};
        }

        $this->answers = $answers;
    }

    public function setAnswerNumber($index)
    {
        $this->answerNumber = $index + 1;
    }

}