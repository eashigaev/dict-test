<?php


namespace Layers\Domain;

use app\models\Mistake;
use Yii;
use app\models\Word;
use yii\base\Event;

class Test extends \ArrayObject
{

    public function toJSON()
    {
        return json_encode($this->getArrayCopy(), JSON_UNESCAPED_UNICODE);
    }

    public static function fromJSON($json)
    {
        $data = array_merge
        (
            [
                'askedQuestions' => [],
                'question' => '',
                'answers' => '',
                'answerNumber' => '',

                'lastAnswered' => null,
                'status' => true,

                'points' => 0,
                'attempts' => 0
            ],
            (array)json_decode($json, true)
        );

        return new static($data);
    }

    public function takeAnswer($number)
    {
        if (!$this['status']) return;

        $number = (int)$number;

        $this['lastAnswered'] = $number === $this['answerNumber'];

        if ($this['lastAnswered'])
        {
            $this['points'] += 1;

            $this->nextQuestion();
        }
        else
        {
            // Saving wrong answer (event?)

            $mistake = new Mistake();

            $mistake->question = $this['question'];

            $mistake->answers = json_encode($this['answers'], JSON_UNESCAPED_UNICODE);

            $mistake->number = $number;

            $mistake->save();


            $this['attempts'] += 1;

            if ($this['attempts'] === 3) $this['status'] = false;
        }
    }

    public function nextQuestion()
    {
        if (!$this['status']) return;

        $direct = rand(0, 1);

        //Direction

        $question = $direct ? new Question('rus', 'eng') : new Question('eng', 'rus');

        //Question

        $word = Word::find()->where(['not in','id', $this['askedQuestions']])->orderBy('RAND()')->one();

        //No questions
        if (is_null($word))
        {
            $this['status'] = false;

            return;
        }


        $question->setText($word);

        //Answer Number

        $answerNumber = rand(0, 3);

        $question->setAnswerNumber($answerNumber);

        //Answers

        $words = (array)Word::find()->where('id != :id', ['id' => $word->id])->orderBy('RAND()')->limit(3)->all();

        array_splice($words, $answerNumber, 0, [$word]);

        $question->setAnswers($words);


        $this['askedQuestions'][] = $question->getId();

        $this['question'] = $question->getText();

        $this['answers'] = $question->getAnswers();

        $this['answerNumber'] = $question->getAnswerNumber();


        return true;
    }

}