<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Functional extends \Codeception\Module
{

    public function request()
    {
        return call_user_func_array([$this->getModule('Yii2'), '_request'], func_get_args());
    }

}
