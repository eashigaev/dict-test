<?php

namespace Step\Functional;

use app\models\Session;
use yii\helpers\Url;

class DictFunctionalTester extends \FunctionalTester
{

    /** Actions */

    public function start($name)
    {
        $I = $this;

        $response = $I->request('GET', Url::toRoute('/api/start'), ['name' => $name]);

        return json_decode($response, true);
    }

    public function getQuestion($token)
    {
        $I = $this;

        $response = $I->request('GET', Url::toRoute('/api/question'), ['token' => $token]);

        return json_decode($response, true);
    }

    public function sendCorrectAnswer($token)
    {
        $I = $this;

        $session = Session::findByTokenOrFail($token);

        $test = $session->getTest();

        return $I->sendAnswer($test['answerNumber'], $token);
    }

    public function sendWrongAnswer($token)
    {
        $I = $this;

        return $I->sendAnswer(0, $token);
    }

    public function sendAnswer($number, $token)
    {
        $I = $this;

        $response = $I->request('GET', Url::toRoute('/api/answer'), ['number' => $number, 'token' => $token]);

        return json_decode($response, true);
    }

    /** Asserts */

    public function assertQuestion($token, $response)
    {
        $I = $this;


        $session = Session::findByTokenOrFail($token);

        $test = $session->getTest();


        $I->assertEqualsQuestions($test, $response);
    }

    public function assertEqualsQuestions($response, $_response, $equals = true)
    {
        $I = $this;

        $I->assertEquals($_response['question'] === $response['question'], $equals);

        $I->assertEquals($_response['answers'] == $response['answers'], $equals);
    }

    public function assertAnswerResult($result, $response)
    {
        $I = $this;

        $I->assertEquals($result, $response['result']);
    }

    public function assertTestStatus($status, $response)
    {
        $I = $this;

        $I->assertEquals($status, $response['status']);
    }

    public function assertPoints($points, $response)
    {
        $I = $this;

        $I->assertEquals($points, $response['points']);
    }

}