<?php

use Step\Functional\DictFunctionalTester as FunctionalTester;
use \app\models\Word;
use \app\models\Session;

/**
 * @author eshigaev
 * @group dict
 */
class DictCest
{

    public function _before(FunctionalTester $I)
    {
        Word::deleteAll();
    }

    public function testSuccessScenario(FunctionalTester $I)
    {
        $I->haveRecord(Word::class, ['eng' => 'One', 'rus' => 'Один']);

        $I->haveRecord(Word::class, ['eng' => 'Two', 'rus' => 'Два']);

        //Starting

        $response = $I->start('Tester');

        $token = $response['token'];

        //Try first question

        $_response = $I->getQuestion($token);

        $I->assertQuestion($token, $_response);

        //Send correct answer

        $response = $I->sendCorrectAnswer($token);

        $I->assertAnswerResult(true, $response);

        $I->assertTestStatus(true, $response);

        //Try next question

        $response = $I->getQuestion($token);

        $I->assertQuestion($token, $response);

        $I->assertEqualsQuestions($response, $_response, false);

        //Send correct answer

        $response = $I->sendCorrectAnswer($token);

        $I->assertAnswerResult(true, $response);

        $I->assertTestStatus(false, $response);

        $I->assertPoints(2, $response);
    }

    public function testEmptyQuestionsList(FunctionalTester $I)
    {
        //Starting

        $response = $I->start('Tester');

        $I->assertTestStatus(false, $response);

        $I->assertPoints(0, $response);
    }

    public function testFailedScenario(FunctionalTester $I)
    {
        $I->haveRecord(Word::class, ['eng' => 'One', 'rus' => 'Один']);

        $I->haveRecord(Word::class, ['eng' => 'Two', 'rus' => 'Два']);

        $I->haveRecord(Word::class, ['eng' => 'Three', 'rus' => 'Три']);

        $I->haveRecord(Word::class, ['eng' => 'Four', 'rus' => 'Четыре']);

        //Starting

        $response = $I->start('Tester');

        $token = $response['token'];

        //Try first question

        $_response = $I->getQuestion($token);

        $I->assertQuestion($token, $_response);

        //Send wrong answer

        $response = $I->sendWrongAnswer($token);

        $I->assertAnswerResult(false, $response);

        $I->assertTestStatus(true, $response);

        //Try same question

        $response = $I->getQuestion($token);

        $I->assertQuestion($token, $response);

        $I->assertEqualsQuestions($response, $_response);

        //Send wrong answer

        $response = $I->sendWrongAnswer($token);

        $I->assertAnswerResult(false, $response);

        $I->assertTestStatus(true, $response);

        //Try same question

        $response = $I->getQuestion($token);

        $I->assertQuestion($token, $response);

        $I->assertEqualsQuestions($response, $_response);

        //Send wrong answer

        $response = $I->sendWrongAnswer($token);

        $I->assertAnswerResult(false, $response);

        $I->assertTestStatus(false, $response);

        $I->assertPoints(0, $response);
    }

}