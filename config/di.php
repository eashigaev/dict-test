<?php

$container = \Yii::$container;

$container->setSingleton
(
    \Layers\Infrastructure\Application\Commands\Contracts\CommandBusInterface::class,
    function ($container, $params, $config)
    {
        return new \Layers\Infrastructure\Application\Commands\CommandBus
        (
            new \Layers\Infrastructure\Application\Containers\Yii2Container(),
            new \Layers\Infrastructure\Application\Commands\NameCommandInflector()
        );
    }
);