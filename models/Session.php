<?php

namespace app\models;

use Layers\Domain\Test;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "session".
 *
 * @property integer $id
 * @property string $token
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['token', 'name'], 'string', 'max' => 255],
            [['test'], 'string'],
            [['token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'name' => 'Name',
            'test' => 'Test',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }

    public static function generateToken()
    {
        do
        {
            $token = \Yii::$app->getSecurity()->generatePasswordHash(time());
        }
        while (static::find()->where(['token' => $token])->one());

        return $token;
    }

    public static function findByTokenOrFail($token)
    {
        $session = static::find()->where(['token' => $token])->one();

        if (!$session)
        {
            throw new Exception('Wrong token');
        }

        return $session;
    }

    /** Test */

    public function getTest()
    {
        return Test::fromJSON($this->test);
    }

    public function saveTest($test)
    {
        $this->test = $test->toJSON();
    }

}
