<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mistakes".
 *
 * @property integer $id
 * @property string $token
 * @property string $text
 */
class Mistake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mistake';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'string', 'max' => 255],
            [['answers'], 'string'],
            [['number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'answers' => 'Answers',
            'number' => 'Number'
        ];
    }
}
